BUCKET_NAME="YOUR-BUCKET-NAME"
BACKUP_PATH_TO_RESTORE="YOUR-PATH-WITH-BACKUP"
BACKUP_FILE="YOUR-DB-FILE-IN-BUCKET"

RESTORE_DB_PATH=${HOME}/$BACKUP_PATH_TO_RESTORE
RESTORE_IMAGES_PATH=/var/www/html/data/images
echo "Dowloading DB..."
gsutil cp gs://$BUCKET_NAME/$BACKUP_FILE $RESTORE_DB_PATH
echo "Dowloading Images..." 
gsutil -m cp -r gs://$BUCKET_NAME/data/images $RESTORE_IMAGES_PATH
echo "Restoring DB..." 
mongorestore --archive=$RESTORE_DB_PATH/$BACKUP_FILE --gzip --drop