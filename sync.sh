#!/bin/bash
# Backup Images and DB and Upload to Bucket at Google Cloud Storage
BUCKET_NAME="YOUR-BUCKET-NAME"
DB_NAME="YOUR-DB-NAME"
CURRENT_TIME=$(date +"%m-%d-%Y")
EXPORT_PATH=${HOME}/sync/$DB_NAME
echo "Exporting DB..."
mongodump --db $DB_NAME --archive=$EXPORT_PATH.gzip --gzip
echo "Uploading DB..."
cd $HOME/sync/
gsutil cp $DB_NAME.gzip gs://$BUCKET_NAME &
echo "Uploading images..."
cd /var/www/html/data/
gsutil -m cp -r images gs://$BUCKET_NAME & 