#!/bin/bash
BUCKET_NAME="YOUR-BUCKET-NAME"
echo "uploading content to CDN..."
cd /var/www/html/data
gsutil -m cp -r images gs://$BUCKET_NAME/data &